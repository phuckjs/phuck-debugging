import { ArcRotateCamera, CascadedShadowGenerator, Color3, DirectionalLight, Engine, HemisphericLight, Mesh, MeshBuilder, Plane, Scene, Space, StandardMaterial, TransformNode, Vector3 } from "babylonjs";
import type { PhuckService } from "../lib/phuck.service";

const Entities = {
    physicalPhone: null as Mesh,
    logicalPhonePointer: null as Mesh,
    pointerIndicator: null as Mesh,
};

var createScene = function (canvas: HTMLCanvasElement, engine: Engine) {
    const scene = new Scene(engine);

    const light = new HemisphericLight('light1', new Vector3(.8, 1, 0), scene);
    light.intensity = 1;
    light.diffuse = Color3.White();
    light.specular = Color3.Black();

    // Character
    const physicalPhone = Entities.physicalPhone = MeshBuilder.CreateBox('player', {
        width: 9,
        height: 4,
        depth: 16,
    }, scene);
    physicalPhone.receiveShadows = false;
    physicalPhone.position.y = 0;
    physicalPhone.material = new StandardMaterial('phoneMat');
    physicalPhone.material.wireframe = true;
    physicalPhone.material.alpha = .2;
    
    const logicalPhonePointer = Entities.logicalPhonePointer = MeshBuilder.CreateBox('logicalPhonePointer', { width: 2, height: 2, depth: 12});
    logicalPhonePointer.lookAt(new Vector3(0, 0, 1));

    const camera = new ArcRotateCamera('camera', -Math.PI / 2, Math.PI * .4, 200, physicalPhone.position);
    camera.fov = .55;
    camera.maxZ = 400;
    camera.attachControl();

    const dir = new Vector3(3, -6, -4);
    const sunlight = new DirectionalLight('sun-directional', dir, scene);
    const shadows = new CascadedShadowGenerator(2048, sunlight, undefined, camera);
    shadows.shadowMaxZ = camera.maxZ;
    shadows.cascadeBlendPercentage = 0; // Perf optimization, zero visual effect I've noticed

    sunlight.intensity = .4;

    shadows.addShadowCaster(physicalPhone);

    const terrainMaterial = new StandardMaterial('materialGround', scene);
    terrainMaterial.diffuseColor = new Color3(.1,.1,.1);
    terrainMaterial.specularColor = Color3.Black();

    const abstractPlane = Plane.FromPositionAndNormal(new Vector3(0, -20, 0), new Vector3(0, 1, 0))
    const terrain = MeshBuilder.CreatePlane('terrain', {
        sourcePlane: abstractPlane,
        size: 100,
    }, scene);
    terrain.material = terrainMaterial;
    terrain.receiveShadows = true;

    // Compass
    const compassPoints = MeshBuilder.CreatePolyhedron('compass-points', { size: 2, type: 1 });
    const northPoint = MeshBuilder.CreateCylinder('compass-north', { diameter: 2, height: .1 });
    northPoint.position.z = 3 - .5;
    compassPoints.position.y = northPoint.position.y = -20;
    const northMat = new StandardMaterial('compass-north-mat');
    northMat.diffuseColor = new Color3(1, 0, 0);
    northPoint.material = northMat;

    const distToProjectionPlane = 100; // this could be modified as a sensitivity adjustment
    const projectionPlane = Plane.FromPositionAndNormal(new Vector3(0, 0, distToProjectionPlane), new Vector3(0, 0, -1));
    const projectionPlaneMesh = MeshBuilder.CreatePlane('projectionSurface', {
        sourcePlane: projectionPlane,
        size: 400,
    });
    projectionPlaneMesh.material = (() => {
        const mat = new StandardMaterial('projectionSurfaceMat');
        mat.diffuseColor = new Color3(0, 0, 0);
        return mat;
    })();

    const pointerIndicator = Entities.pointerIndicator = MeshBuilder.CreateSphere('poinerIndicator', {
        diameter: 2,
    });
    pointerIndicator.material = (() => {
        const mat = new StandardMaterial('pointerIndicatorMat');
        mat.diffuseColor = new Color3(0, 0, 0);
        mat.emissiveColor = new Color3(1, 0, 0);
        return mat;
    })();
    pointerIndicator.position = physicalPhone.forward.projectOnPlane(projectionPlane, Vector3.Zero());

    return [scene] as const;
}

export async function main3d(service: PhuckService) {
    const canvas = document.createElement('canvas');
    document.body.appendChild(canvas);

    var engine = new Engine(canvas, true, { preserveDrawingBuffer: false, stencil: true }, true);

    var [scene] = createScene(canvas, engine);

    engine.runRenderLoop(function () {
        scene.render();
    });

    window.addEventListener('resize', function () {
        engine.resize();
    });

    service.orientation$.subscribe((data) => {
        if (!data.phoneRotation || !data.pointer) return;

        Entities.physicalPhone.rotation = new Vector3(
            data.phoneRotation.x,
            data.phoneRotation.y,
            data.phoneRotation.z,
        );

        Entities.logicalPhonePointer.rotation = new Vector3(
            data.pointer.rotation.x,
            data.pointer.rotation.y,
            data.pointer.rotation.z,
        );

        Entities.pointerIndicator.position = new Vector3(
            data.pointer.position.x,
            data.pointer.position.y,
            100,
        );
    });

    return engine;
}