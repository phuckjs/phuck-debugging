import { navigate, type Route } from "./lib/Router/Router.service";
import Home from './Views/Home.svelte';

const Routes: Route[] = [
    { path: '/home', component: Home },
    { path: '/', execute: () => navigate('/home'), component: null }
];

export default Routes;
