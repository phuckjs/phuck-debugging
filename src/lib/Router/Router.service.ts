import Navigo, { type BeforeHook, type LeaveHook, type Match } from 'navigo';
import { BehaviorSubject } from 'rxjs';

export type ActivatedRoute = Match;
export type SvelteComponent = any;
export interface Route {
    path: string;
    component: SvelteComponent;
    overlay?: SvelteComponent;
    pre?: () => boolean;
    execute?: (ar: ActivatedRoute) => void;
}
export type SetView = (component: SvelteComponent, overlay: SvelteComponent) => void;
export const activatedRoute$ = new BehaviorSubject<ActivatedRoute>(null);

const router = new Navigo('/', { hash: true });
router.notFound(() => {
    console.log('Route not found');
    router.navigate('/');
});

let activatedRoute = null;
let leave: LeaveHook | null = null;
export function withRoutes(routes: Route[], setView: SetView) {
    if (leave) {
        router.hooks({ leave });
    }

    routes.forEach(route => {
        router.on(route.path, (match) => {
            activatedRoute = match;
            activatedRoute$.next(activatedRoute);

            if (route.execute) {
                route.execute(activatedRoute);
            } else {
                if (route.pre) {
                    if (route.pre()) {
                        setView(route.component, route.overlay);
                    }
                } else {
                    setView(route.component, route.overlay);
                }
            }
        });
    });

    router.resolve();
}

export function addGlobalLeaveHook(_leave: LeaveHook) {
    leave = _leave;
}

export function getActivatedRoute(): ActivatedRoute {
    return activatedRoute;
}

export function navigate(url, opts?) {
    router.navigate(url, opts);
}

export function back() {
    window.history.back();
}
