import { Engine, Plane, Scene, Space, Tools, TransformNode, UniversalCamera, Vector3,  } from "babylonjs";
import { BehaviorSubject, Subject, type Observable } from "rxjs";
import { joinRoom, selfId, type Room } from "trystero";

export type ConnectedPeers = Set<string>;

export class PhuckService {
    orientation$ = new BehaviorSubject<PhuckOrientationData & PhuckComputedData>({
        alpha: 0,
        beta: 0,
        gamma: 0,
        pointer: null,
        phoneRotation: null,
    });

    peers = new Set<string>();
    peers$ = new BehaviorSubject<ConnectedPeers>(this.peers);

    private engine: Engine;
    private scene: Scene;
    private initialPhoneAlpha: number = null;
    private pointerResetting = false;

    private deviceTn: TransformNode;
    private logicalPointerTn: TransformNode;
    private projectionPlane: Plane;

    startListening(appId: string, roomId: string): Observable<ConnectedPeers> {

        const room = joinRoom({ appId }, roomId);
    
        const [sendHost] = room.makeAction('announceHost');
        const [sendPhuck, getPhuck] = room.makeAction('phuck');
    
        room.onPeerJoin(peerId => {
            console.log(`${peerId} joined`);
            sendHost(selfId, peerId).then(() => console.log('Announced Host'));
            
            this.peers.add(peerId);
            this.peers$.next(this.peers);
        });
    
        room.onPeerLeave(peerId => {
            this.peers.delete(peerId);
            this.peers$.next(this.peers);
        });
    
        getPhuck((data, peerId) => this.onPhuckOrientationData(data as any, peerId));
    
        return this.peers$;
    }

    resetPointer() {
        this.initialPhoneAlpha = null;
        this.pointerResetting = true;
    }

    // Temporary until I move this to the handheld device
    setEngine(engine: Engine) {
        this.engine = engine;
        this.scene = new Scene(engine, { virtual: true });

        this.deviceTn = new TransformNode('physicalPhoneTn', this.scene, true);
        this.logicalPointerTn = new TransformNode('logicalPhoneTN', this.scene, true);
        this.logicalPointerTn.setParent(this.deviceTn);

        const distToProjectionPlane = 100; // this could be modified as a sensitivity adjustment
        this.projectionPlane = Plane.FromPositionAndNormal(
            new Vector3(0, 0, distToProjectionPlane),
            new Vector3(0, 0, -1),
        );
    }

    private onPhuckOrientationData(data: PhuckOrientationData, peerId: string) {
        const skipEmission = this.handlePointerResetSteps(data);

        this.deviceTn.rotation = new Vector3(
            Tools.ToRadians(data.beta) * -1,
            Tools.ToRadians(this.initialPhoneAlpha - data.alpha),
            Tools.ToRadians(data.gamma) * -1,
        );

        // Soooo, TransformNode's do a bunch of cacheing to optimize multiple updates between frames
        //   This is bad for me because idgaf about rendering, i just want the math.
        //   Gonna recompute the world Matrix (implicitly updates parent) this alone will only update
        //     the tn.rotationQuat but NOT tn.rotation.  Instead we convert the rotation quat to a vec3 ourselves
        this.logicalPointerTn.computeWorldMatrix(true);
        const logicalPhoneTnRotation = this.logicalPointerTn.absoluteRotationQuaternion.toEulerAngles();

        const pointerForward = this.logicalPointerTn.forward;
        const pointerPosition = pointerForward.projectOnPlane(this.projectionPlane, Vector3.Zero());

        if (skipEmission) return;

        this.orientation$.next(Object.assign({}, data, {
            phoneRotation: this.deviceTn.rotation,
            pointer: {
                position: pointerPosition,
                rotation: logicalPhoneTnRotation,
            },
        }));
    }

    // For now this needs to be a 2 step process
    //   1. First invoke will reestablish initialPhoneAlpha AND update the nodes once
    //   2. With our new world view, force the pointer to lookat 0
    //   bug reproduce: Connect device, reorient device while screen turned off, reconnect, and resetPointer.
    //     The pointer will point forward but device orientation data will cause the deviceTn to no longer match
    //     reality.  Seems to be a browser thing maybe. Makes controlling the pointer non-functional
    private handlePointerResetSteps(data: PhuckOrientationData) {
        if (this.initialPhoneAlpha !== null && this.pointerResetting) {
            this.logicalPointerTn.lookAt(new Vector3(0, 0, 1), 0, 0, 0, Space.WORLD);
            this.pointerResetting = false;

            return true;
        }

        if (this.initialPhoneAlpha === null) {
            this.initialPhoneAlpha = data.alpha;

            return true;
        }

        return false;
    }
}

export interface PhuckOrientationData {
    alpha: number;
    beta: number;
    gamma: number;
}

export interface PhuckComputedData {
    phoneRotation: { x: number, y: number, z: number }, 
    pointer: {
        position: { x: number, y: number },
        rotation: { x: number, y: number, z: number },
    };
}